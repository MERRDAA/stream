package streamApps;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.ForeachAction;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Reducer;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.Windowed;
import org.json.JSONException;
import org.json.JSONObject;




import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class DetectNoise {

	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-pipe");
		props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "deti-engsoft-01.ua.pt:9092");
		props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.Long().getClass());

		final Serde<String> stringSerde = Serdes.String();


		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, String> kafkaInput = builder.stream("401_all", Consumed.with(stringSerde, stringSerde) );
		KTable<Windowed<String>, Long> countSensor =
				kafkaInput
				.map((k,v) ->  {
					try {
						return KeyValue.pair((String)(new JSONObject(v).getString("sensor")), new Double((new JSONObject(v).getDouble("volume"))*100).longValue());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return null;
				})
				.groupByKey()
				.windowedBy(TimeWindows.of(TimeUnit.SECONDS.toMillis(30)).until(TimeUnit.SECONDS.toMillis(30)).advanceBy(TimeUnit.SECONDS.toMillis(30)))
				.count();
		
		KTable<Windowed<String>, Long> sumSound =
				kafkaInput
				.map((k,v) ->  {
					try {
						return KeyValue.pair((String)(new JSONObject(v).getString("sensor")), new Double((new JSONObject(v).getDouble("volume"))*100).longValue());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return null;
				})
				.groupByKey()
				.windowedBy(TimeWindows.of(TimeUnit.SECONDS.toMillis(30)).until(TimeUnit.SECONDS.toMillis(30)).advanceBy(TimeUnit.SECONDS.toMillis(30)))
				.reduce(new Reducer<Long>() {

					@Override
					public Long apply(Long arg0, Long arg1) {
						return arg0 + arg1;
					}
				});

		KStream<String, Long> realTime=kafkaInput
				.map((k,v) ->  {
					try {
						return KeyValue.pair((String)(new JSONObject(v).getString("sensor")), new Double((new JSONObject(v).getDouble("volume"))*100).longValue());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return null;
				});
		realTime.print();
		realTime.to("401_realTime");
				
		KTable<Windowed<String>, Long> average = 
				sumSound.leftJoin(countSensor, (sum,count) -> sum.longValue()/count.longValue());

		KStream<String, Long> alert=
				average.toStream((wk, v) -> wk.key()).filter((k,v) -> v>50 );
		alert.to("401_alerts");
		
		KStream<String, Long> persistence=
				average.toStream((wk, v) -> wk.key());

		persistence.to("401_windows");

		KafkaStreams streams = new KafkaStreams(builder.build(), props);

		streams.start();
	}

}
